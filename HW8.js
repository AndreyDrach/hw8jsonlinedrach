// 1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// Это программный интерфейс, позволяющий программам и скриптам получить доступ к содержимому HTML- документов, а также вносить изменения.

// 2. Какая разница между свойствами HTML-элементов innerHTML и innerText?
// innerHTML Возвращает все дочерние элементы, вложенные внутри тега (тег HTML + текстовое содержимое)
// innerText Возвращает текстовое содержимое дочерних элементов, вложенных внутри метки.

// 3. Как можно обратится к элементу страницы с помощью JS? Какой способ предпочтительнее
// По названию класса, по ID, по имени, по тегу.  Самым предпочтительным способом считаю querySelector или querySelectorAll

const paragraph = document.querySelectorAll("p");
paragraph.forEach((paragraph) => paragraph.classList.add("paparagraph-style"));

const elemIdOptionsList = document.getElementById("optionsList");
console.log(elemIdOptionsList);
console.log(elemIdOptionsList.parentNode);
console.log(elemIdOptionsList.childNodes);

const elemIdTestParagraph = document.getElementById("testParagraph");
elemIdTestParagraph.textContent = "This is a paragraph";

const classNameMainHeader = document.querySelector(".main-header");
const classNameMainHeaderLi = classNameMainHeader.querySelectorAll("li");
console.log(classNameMainHeaderLi);
classNameMainHeaderLi.forEach((elem) => (elem.className = "nav-item"));
console.log(classNameMainHeaderLi);

const elemsClassNameSectionTitle = document.querySelectorAll(".section-title");
console.log(elemsClassNameSectionTitle);
elemsClassNameSectionTitle.forEach((elem) =>
  elem.classList.remove("section-title")
);
console.log(elemsClassNameSectionTitle);
